import {AlertService, IAlertService, Options} from '@spryrocks/alert-react-native';
import {ContainerModule} from 'inversify';

const createService = (options: Options | undefined) => {
  const service = new AlertService();
  if (options) {
    service.setOptions(options);
  }
  return service;
};

export const createModule = (options?: Options) =>
  new ContainerModule((bind) => {
    bind(IAlertService).toConstantValue(createService(options));
  });
