import {Initializable} from '@spryrocks/services-core';

export type LanguageChangedCallback = (lang: string) => void;

export abstract class ILocalizationService implements Initializable {
  abstract initialize(): Promise<void>;

  abstract setLanguage(lang: string): Promise<void>;

  abstract addOnLanguageChanged(callback: LanguageChangedCallback): void;

  abstract removeOnLanguageChanged(callback: LanguageChangedCallback): void;
}
