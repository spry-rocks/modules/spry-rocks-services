export * from './ILocalizationService';
export {default as LanguageDetector} from './LanguageDetector';
export {LocalizationService} from './LocalizationService';
