import i18next, {Resource} from 'i18next';
import {ILocalizationService, LanguageChangedCallback} from './ILocalizationService';

export class LocalizationService extends ILocalizationService {
  private readonly languageChangedCallbacks: LanguageChangedCallback[] = [];

  constructor(
    private readonly locale: string | undefined,
    private readonly resources: Resource,
  ) {
    super();
  }

  async initialize() {
    try {
      if (!this.locale) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (__DEV__) {
          // noinspection ExceptionCaughtLocallyJS
          throw new Error("Can't detect language");
        }
      }

      await i18next.init({
        react: {
          // useSuspense: r,
          // wait: false,
        },
        resources: this.resources,
        fallbackLng: 'en',
        lng: this.locale,
        keySeparator: false,
        returnNull: false,
      });

      i18next.on('languageChanged', this.onLanguageChanged.bind(this));
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }

  async setLanguage(lang: string) {
    await i18next.changeLanguage(lang);
  }

  addOnLanguageChanged(callback: LanguageChangedCallback) {
    this.languageChangedCallbacks.push(callback);
  }

  removeOnLanguageChanged(callback: LanguageChangedCallback) {
    const index = this.languageChangedCallbacks.indexOf(callback);
    if (index > -1) {
      this.languageChangedCallbacks.splice(index, 1);
    }
  }

  onLanguageChanged(lng: string) {
    for (const callback of this.languageChangedCallbacks) {
      callback(lng);
    }
  }
}
