import {IPreferencesService, ParamListBase} from './IPreferencesService';
import {IStorageBackend} from '@react-native-community/async-storage';

type ItemModel<T> = {
  value: T | undefined;
};

export class PreferencesService<
  ParamList extends ParamListBase,
> extends IPreferencesService<ParamList> {
  private readonly storage: IStorageBackend;

  constructor(storage: IStorageBackend) {
    super();
    this.storage = storage;
  }

  async set<Key extends keyof ParamList>(key: Key, value: ParamList[Key] | undefined) {
    const model: ItemModel<ParamList[Key]> = {value};
    await this.storage.setSingle(key, JSON.stringify(model));
  }

  async get<Key extends keyof ParamList>(key: Key) {
    const valueS: string | null = await this.storage.getSingle(key);
    if (valueS === null) return undefined;
    const model: ItemModel<ParamList[Key]> = JSON.parse(valueS);
    return model.value;
  }
}
