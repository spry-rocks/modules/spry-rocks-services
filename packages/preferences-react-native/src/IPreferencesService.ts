export type ParamListBase = Record<string, unknown>;

export abstract class IPreferencesService<ParamList extends ParamListBase> {
  abstract set<Key extends keyof ParamList>(
    key: Key,
    value: ParamList[Key] | undefined,
  ): Promise<void>;

  abstract get<Key extends keyof ParamList>(
    key: Key,
  ): Promise<ParamList[Key] | undefined>;
}
