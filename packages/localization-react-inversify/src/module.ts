import {
  ILocalizationService,
  LanguageDetector,
  LocalizationService,
} from '@spryrocks/localization-react-native';
import {ContainerModule} from 'inversify';
import {Resource} from 'i18next';

export const CurrentLocale = Symbol.for('LocalizationService.CurrentLocale');
export const Resources = Symbol.for('LocalizationService.Resources');
//
export const createModule = (options?: {
  translations?: Resource;
  currentLocale?: string;
  localizationService?: {constantValue?: ILocalizationService};
}) =>
  new ContainerModule((bind) => {
    bind(CurrentLocale).toDynamicValue(
      () => options?.currentLocale ?? LanguageDetector.getCurrentLocale(),
    );
    bind(Resources).toConstantValue(options?.translations);
    if (options?.localizationService?.constantValue) {
      bind(ILocalizationService).toConstantValue(
        options.localizationService.constantValue,
      );
    } else {
      bind(ILocalizationService).toDynamicValue((context) => {
        const currentLocale: string | undefined = context.container.get(CurrentLocale);
        const resources: Resource = context.container.get(Resources);
        return new LocalizationService(currentLocale, resources);
      });
    }
  });
