import {Options} from './Options';

export type AlertCallback = () => void;

export type Button = {
  text: string;
  onPress?: AlertCallback;
};

export type ShowMessage = {
  title: string;
  message: string;
  positiveButton?: Button;
  negativeButton?: Button;
};

export type ShowError = {
  message: string;
  title?: string;
  positiveButton?: Button;
};

export interface IAlertInterceptors {
  showMessage?(props: ShowMessage): boolean;
}

export abstract class IAlertService {
  abstract setOptions(options: Options): void;

  abstract showMessage(props: ShowMessage): void;

  abstract showError(props: ShowError): void;

  abstract addAlertInterceptors(interceptors: IAlertInterceptors): void;

  abstract removeAlertInterceptors(interceptors: IAlertInterceptors): void;
}
