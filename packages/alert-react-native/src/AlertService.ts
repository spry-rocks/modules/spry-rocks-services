import {Alert, AlertButton, Platform} from 'react-native';
import {
  Button,
  IAlertInterceptors,
  IAlertService,
  ShowError,
  ShowMessage,
} from './IAlertService';
import {Options} from './Options';

// declaration for web version
declare const alert: (message: string) => void;

const createButton = ({
  button,
  style,
}: {
  button: Button;
  style: 'default' | 'cancel' | 'destructive' | undefined;
}): AlertButton => ({
  text: button.text,
  style,
  onPress: button.onPress,
});

export class AlertService extends IAlertService {
  private options: Options | undefined;

  private interceptors: IAlertInterceptors[] = [];

  setOptions(options: Options) {
    this.options = options;
  }

  // eslint-disable-next-line class-methods-use-this
  showMessage(props: ShowMessage) {
    const {title, message, positiveButton, negativeButton} = props;

    for (const interceptor of this.interceptors) {
      if (interceptor.showMessage && interceptor.showMessage(props)) {
        return;
      }
    }

    if (Platform.OS === 'web') {
      alert(`${title}\n${message}`);
      return;
    }

    const buttons: AlertButton[] = [];
    if (positiveButton) {
      buttons.push(createButton({button: positiveButton, style: 'default'}));
    }
    if (negativeButton) {
      buttons.push(createButton({button: negativeButton, style: 'cancel'}));
    }
    Alert.alert(title, message, buttons);
  }

  public showError({message, positiveButton, title}: ShowError) {
    this.showMessage({
      title: title ?? this.options?.defaultTitle ?? 'Error',
      message,
      positiveButton: positiveButton ?? this.options?.defaultPositive ?? {text: 'OK'},
    });
  }

  // region Interceptors
  addAlertInterceptors(interceptors: IAlertInterceptors) {
    this.interceptors.push(interceptors);
  }

  removeAlertInterceptors(interceptors: IAlertInterceptors) {
    const index = this.interceptors.indexOf(interceptors);
    if (index > -1) {
      this.interceptors.splice(index, 1);
    }
  }
  // endregion
}
