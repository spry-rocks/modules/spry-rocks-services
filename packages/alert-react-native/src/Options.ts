import {Button} from './IAlertService';

export interface Options {
  defaultTitle: string;
  defaultPositive?: Button;
}
