import {ContainerModule, interfaces} from 'inversify';
import {
  IPreferencesService,
  ParamListBase,
  PreferencesService,
} from '@spryrocks/preferences-react-native';
import {IStorageBackend} from '@react-native-community/async-storage';
import ServiceIdentifier = interfaces.ServiceIdentifier;

export const createModule = <ParamList extends ParamListBase>(options: {
  storage?: IStorageBackend;
  preferencesService: {
    identifier: ServiceIdentifier<IPreferencesService<ParamList>>;
    constantValue?: IPreferencesService<ParamList>;
  };
}) =>
  new ContainerModule((bind) => {
    if (options?.preferencesService?.constantValue) {
      bind(options.preferencesService.identifier).toConstantValue(
        options.preferencesService.constantValue,
      );
    } else {
      bind(options.preferencesService.identifier).toDynamicValue(() => {
        if (!options?.storage) throw Error('You should provide options.storage instance');
        return new PreferencesService(options.storage) as IPreferencesService<ParamList>;
      });
    }
  });
