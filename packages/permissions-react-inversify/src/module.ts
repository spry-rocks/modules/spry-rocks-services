import {
  IPermissionsService,
  PermissionsService,
} from '@spryrocks/permissions-react-native';
import {ContainerModule} from 'inversify';

export const createModule = (options?: {
  permissionsService?: {constantValue?: IPermissionsService};
}) =>
  new ContainerModule((bind) => {
    if (options?.permissionsService?.constantValue) {
      bind(IPermissionsService).toConstantValue(options.permissionsService.constantValue);
    } else {
      bind(IPermissionsService).toConstantValue(new PermissionsService());
    }
  });
