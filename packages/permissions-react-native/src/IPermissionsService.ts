export type RequestPermissionResult =
  | 'unavailable'
  | 'blocked'
  | 'denied'
  | 'granted'
  | 'limited';

export type RequestLocationPermissionsResult = {
  location: RequestPermissionResult;
  background: RequestPermissionResult | 'notSupported' | 'notRequested';
};

export type RequestLocationPermissionsOptions = {
  background?: boolean;
};

export abstract class IPermissionsService {
  abstract requestLocationPermissions(
    options?: RequestLocationPermissionsOptions,
  ): Promise<RequestLocationPermissionsResult>;

  abstract requestMediaPermission(): Promise<RequestPermissionResult>;

  abstract openSettings(): Promise<void>;
}
