/* eslint-disable class-methods-use-this */
import {
  IPermissionsService,
  RequestLocationPermissionsOptions,
  RequestLocationPermissionsResult,
} from './IPermissionsService';
import {
  PERMISSIONS,
  request,
  requestMultiple,
  openSettings as RNP_openSettings,
} from 'react-native-permissions';
import {Platform} from 'react-native';

export class PermissionsService extends IPermissionsService {
  // region Location
  private static async requestLocationPermissionAndroid() {
    const permission = PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    return request(permission);
  }

  private static async requestLocationPermissionsAndroidBackground() {
    const permissions = [
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
    ];
    const results = await requestMultiple(permissions);
    return {
      location: results[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION],
      background: results[PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION],
    };
  }

  private static async requestLocationPermissionIos() {
    const permission = PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
    return request(permission);
  }

  async requestLocationPermissions(
    options?: RequestLocationPermissionsOptions,
  ): Promise<RequestLocationPermissionsResult> {
    const os = Platform.OS;
    if (os === 'android') {
      if (options?.background) {
        const result =
          await PermissionsService.requestLocationPermissionsAndroidBackground();
        return {
          location: result.location,
          background: result.background,
        };
      }
      const result = await PermissionsService.requestLocationPermissionAndroid();
      return {
        location: result,
        background: 'notRequested',
      };
    }
    if (os === 'ios') {
      const result = await PermissionsService.requestLocationPermissionIos();
      return {
        location: result,
        background: 'notSupported',
      };
    }

    throw new Error(`Not supported OS: ${os}`);
  }
  // endregion

  // region Media
  static async requestMediaPermissionAndroid() {
    const permission = PERMISSIONS.ANDROID.CAMERA;
    return request(permission);
  }

  static async requestMediaPermissionIos() {
    const permission = PERMISSIONS.IOS.CAMERA;
    return request(permission);
  }

  async requestMediaPermission() {
    if (Platform.OS === 'android') {
      return PermissionsService.requestMediaPermissionAndroid();
    }
    if (Platform.OS === 'ios') {
      return PermissionsService.requestMediaPermissionIos();
    }
    throw new Error(`Platform ${Platform.OS} not supported`);
  }
  // endregion

  // region Settings
  async openSettings() {
    await RNP_openSettings();
  }
  // endregion
}
